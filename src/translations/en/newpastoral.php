<?php
/**
 * Newpastoral plugin for Craft CMS 3.x
 *
 * Plugin to handle Newpastoral API
 *
 * @link      http://pleodigital.com
 * @copyright Copyright (c) 2019 PLEO Digital
 */

/**
 * Newpastoral en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('newpastoral', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    PLEO Digital
 * @package   Newpastoral
 * @since     1.0.0
 */
return [
    'Newpastoral plugin loaded' => 'Newpastoral plugin loaded',
];
