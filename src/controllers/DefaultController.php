<?php
/**
 * Newpastoral plugin for Craft CMS 3.x
 *
 * Plugin to handle Newpastoral API
 *
 * @link      http://pleodigital.com
 * @copyright Copyright (c) 2019 PLEO Digital
 */

namespace pleodigital\newpastoral\controllers;

use pleodigital\newpastoral\Newpastoral;

use Craft;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    PLEO Digital
 * @package   Newpastoral
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected array|bool|int $allowAnonymous = ['index', 'propose-host', 'propose', 'propose-user','accept-user', 'reset-password'];
    private $apiUrl = '';

    public function init() : void
    
	{

		parent :: init();

        $this -> apiUrl = getenv('NEWPASTORAL_API_ENDPOINT');

    }
    // Public Methods
    // =========================================================================

    public function actionPropose()
    {
        
        $postArray = Craft::$app ->getRequest() -> getBodyParams();
        $postArray['newUsers'][0]['type'] = 4;

        $url = $this -> apiUrl . 'parishes/propose';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postArray));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept-Language: ' . Craft::$app->language . '-' . strtoupper( Craft::$app->language )
        ));
        $response = curl_exec($curl);

        curl_close($curl);

        if ( $response === FALSE )
            Craft::$app->getSession() -> setError( Craft :: t( 'app', "Nie udało się zgłosić parafii. Uzupełnij prawidłowo formularz." ) );
        else
            Craft::$app->getSession() -> setNotice( Craft :: t( 'app', "Pomyślnie zgłoszono parafię." ) );
    
    }

    public function actionProposeUser()
    {
        
        $postArray = Craft::$app ->getRequest() -> getBodyParams();
        $postArray['type'] = 1;
        $postArray['status'] = 0;
        $postArray['verified'] = 1;

        $url = $this -> apiUrl . 'user/registerWithPin';

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postArray));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept-Language: ' . Craft::$app->language . '-' . strtoupper( Craft::$app->language )
        ));
        $response = curl_exec($curl);


        curl_close($curl);

        if ( $response === FALSE )
            Craft::$app-> getSession()-> setError( Craft :: t( 'app', "Nie udało się zgłosić nowego członka małej grupy. Uzupełnij prawidłowo formularz." ) );
        else
            Craft::$app-> getSession()-> setNotice( Craft :: t( 'app', "Pomyślnie zgłoszono nowego członka małej grupy." ) );
    
    }

    public function actionAcceptUser()
    {

        $postArray = Craft::$app ->getRequest() -> getBodyParams();
        $url = $this -> apiUrl . 'user/activate';

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept-Language: ' . Craft::$app->language . '-' . strtoupper( Craft::$app->language )
        ));

        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postArray));

        $response = curl_exec($curl);

        curl_close($curl);

        if ( $response === FALSE )
            Craft::$app->getSession()-> setError( Craft :: t( 'app', "Nie udało się utworzyć konta. Uzupełnij prawidłowo formularz." ) );
        else
            Craft::$app->getSession()-> setNotice( Craft :: t( 'app', "Pomyślnie utworzono konto." ) );


    }

    public function actionProposeHost() { 
        
        $postArray = Craft::$app->getRequest() -> getBodyParams();
        $url = $this -> apiUrl . 'parishes/proposeHost';

        $postArray['newUsers']['type'] = 2;
        $postArray['newUsers']['status'] = 0;
        $postArray['newUsers']['verified'] = 0; 
        
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postArray));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept-Language: ' . Craft::$app->language . '-' . strtoupper( Craft::$app->language )
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        if ( $response === FALSE )
            Craft::$app -> getSession() -> setError( Craft :: t( 'app', "Nie udało się zgłosić gospodarza. Uzupełnij prawidłowo formularz." ) );
        else
            Craft::$app -> getSession() -> setNotice( Craft :: t( 'app', "Pomyślnie zgłoszono konto gospodarza." ) );


    }

    public function actionResetPassword() {
        
        $postArray = Craft::$app ->getRequest() -> getBodyParams();
        $url = $this -> apiUrl . 'user/setNewPass';
        
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postArray));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept-Language: ' . Craft::$app->language . '-' . strtoupper( Craft::$app->language )
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        if ( $response === FALSE )
            Craft::$app -> getSession() -> setError( Craft :: t( 'app', "Nie udało się zmienić hasła. Uzupełnij prawidłowo formularz." ) );
        else
            Craft::$app -> getSession() -> setNotice( Craft :: t( 'app', "Pomyślnie zmieniono hasło." ) );


    }

}
